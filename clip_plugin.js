videojs.registerPlugin('clip', function () {
  const player = this;
  this.on('loadedmetadata', () => {
    const playheadWell = this.el_.querySelector(
      '.vjs-progress-control.vjs-control'
    );

    if (player.el_.hasAttribute('data-start-at')) {
      player.currentTime(player.el_.attributes['data-start-at'].value);
    }

    if (player.el_.hasAttribute('data-stop-at')) {
      let pauseState = false;

      player.on('timeupdate', function (e) {
        const timestamp = Math.floor(player.currentTime());

        if (
          timestamp >= player.el_.attributes['data-stop-at'].value &&
          !pauseState
        ) {
          player.pause();
          pauseState = true;
        } else if (timestamp < player.el_.attributes['data-stop-at'].value) {
          pauseState = false;
        }
      });
    }
  });
});
