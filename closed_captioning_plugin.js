videojs.registerPlugin('closeCaptioning', function () {
  var player = this;
  var userLanguage = '';
  // However, if the `lang` attribute should be ignored, default to the browser or OS settings.  This option
  // should be used when there's no language switcher or other page translation, meaning that a user who prefers
  // German and wants the subtitles would still be looking at a page in English because no German page exists.
  if (player.el_.hasAttribute('data-use-page-lang')) {
    // Get the user's preferred language.
    // By default, use the `lang` attribute of the page or surrounding markup (player.language().
    userLanguage = document.documentElement.lang;
  } else {
    userLanguage = window.navigator.language;
  }
  userLanguage = userLanguage.substr(0, 2);

  // Display the caption in the user's language by default (if captions exist).
  var trackLanguage;
  var textTracks = player.textTracks();

  var languageArray = [];
  var languageOrder = document.querySelectorAll(
    '.language-switcher-language-url ul li ul li'
  );
  for (var lo = 0; lo < languageOrder.length; lo++) {
    languageArray.push(languageOrder[lo].getAttribute('hreflang'));
  }

  for (var i = 0; i < textTracks.length; i++) {
    var label = textTracks[i].label;
    trackLanguage = textTracks[i].language.substr(0, 2);
    for (var la = 0; la < languageArray.length; la++) {
      if (trackLanguage === languageArray[la]) {
        var languageCaptionMenu = player.el_.querySelectorAll(
          '.vjs-subs-caps-button .vjs-menu ul li'
        );
        for (var lcm = 0; lcm < languageCaptionMenu.length; lcm++) {
          var language = player.language_ ? player.language_ : 'en';
          var stringClean = /\n\s+/;
          if (player.languages_[language]) {
            var menuLabel = languageCaptionMenu[lcm]
              .querySelector('.vjs-menu-item-text')
              .innerText.replace(player.languages_[language].Captions, '')
              .replace(stringClean, '');
            if (menuLabel === label) {
              var orderCSS = 'order:' + la + ';';
              languageCaptionMenu[lcm].setAttribute('style', orderCSS);
            }
          }
        }
      }
    }
    if (textTracks[i].kind == 'subtitles' && trackLanguage == userLanguage) {
      // We found a track that should be the default.
      textTracks[i].mode = 'showing';
    } else {
      if (!textTracks[i].kind == 'chapters') {
        textTracks[i].mode = 'disabled';
      }
    }
  }
});
