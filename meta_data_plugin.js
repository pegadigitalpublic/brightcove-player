videojs.registerPlugin('metaData', function (title, duration, player, preview) {
  const eventManager = function (e) {
    e.on('play', function () {
      if (e.el().querySelector('.video-meta-container__item--title')) {
        e.el()
          .querySelector('.video-meta-container__item--title')
          .classList.add('is-playing');
      }
      if (e.el().querySelector('.video-meta-container__item--duration')) {
        e.el()
          .querySelector('.video-meta-container__item--duration')
          .classList.add('is-playing');
      }
      if (e.el().querySelector('.video-meta-container__item--preview')) {
        e.el()
          .querySelector('.video-meta-container__item--preview')
          .classList.add('is-playing');
      }
    });
    e.on('pause', function () {
      if (e.el().querySelector('.video-meta-container__item--title')) {
        e.el()
          .querySelector('.video-meta-container__item--title')
          .classList.remove('is-playing');
      }
      if (e.el().querySelector('.video-meta-container__item--preview')) {
        e.el()
          .querySelector('.video-meta-container__item--preview')
          .classList.remove('is-playing');
      }
    });
  };

  let container;

  const createNode = function (className, content) {
    const returnValue = document.createElement('div');
    returnValue.textContent = content;
    returnValue.setAttribute(
      'class',
      'video-meta-container__item video-meta-container__item--' + className
    );
    return returnValue;
  };

  const formatVideoDuration = function (seconds) {
    const mm = Math.floor(seconds / 60) || 0;
    const ss = ('0' + Math.floor(seconds % 60)).slice(-2);
    return mm + ':' + ss;
  };

  const metaTitle = createNode('title', title);

  const metaPreview = createNode('preview', 'Preview');

  const metaDuration = createNode(
    'duration',
    formatVideoDuration(Math.floor(duration))
  );

  if (title || preview || duration) {
    container = document.createElement('div');
    container.setAttribute('class', 'video-meta-container');
    container.getElementsByClassName;
    player.el().prepend(container);
  }

  const videoMediaContainer = player
    .el()
    .getElementsByClassName('video-meta-container');

  if (title) {
    videoMediaContainer[0].prepend(metaTitle);
  }

  if (preview) {
    videoMediaContainer[0].prepend(metaPreview);
  }

  if (duration) {
    videoMediaContainer[0].prepend(metaDuration);
  }

  eventManager(player);
});
