videojs.registerPlugin('errorReporting', function (player, err, sendbeacon) {
  function playbackResponse() {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {};
    xhr.open('GET', 'https://edge.api.brightcove.com/playback/v1');
    xhr.send();
    return xhr;
  }

  if (typeof sendbeacon === 'object') {
    for (let trackerIndex in sendbeacon) {
      let trackerService = sendbeacon[trackerIndex].sendbeacon || false;
      let trackerServiceParams = sendbeacon[trackerIndex].getFormattedError || false;

      if (trackerService && trackerServiceParams) {
        let formattedError = trackerServiceParams(playbackResponse);
        trackerService.apply(this, formattedError);
      }
    }
  }
  else if (typeof sendbeacon === 'function') {
    // Backward compatibility support.
    sendbeacon(
      'asset engagement',
      'VID - Error',
      player.mediainfo.name + ' | ' + player.mediainfo.id,
      true,
      new Date() +
      ' | ' +
      player.error().code +
      ' | ' +
      player.error().message +
      ' | ' +
      player.mediainfo.accountId +
      ' | ' +
      player.mediainfo.id +
      ' | ' +
      player.mediainfo.playerId +
      ' | ' +
      playbackResponse.responseURL +
      ', ' +
      playbackResponse.responseType +
      ', ' +
      playbackResponse.response
    );
  }
});
