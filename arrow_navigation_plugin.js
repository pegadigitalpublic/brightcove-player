videojs.registerPlugin('arrowNavigation', function () {
  var player = this;
  var utilities = {
    jumpAmount: 5,
    controlBar: player.$('.vjs-control-bar'),
    insertBeforeNode: player.$('.vjs-volume-panel'),
    backButton: document.createElement('button'),
    forwardButton: document.createElement('button'),
    backButtonImage: document.createElement('img'),
    forwardButtonImage: document.createElement('img'),
  };

  // Build the back button Image
  utilities.backButtonImage.setAttribute(
    'src',
    'https://player.support.brightcove.com/assets/images/code-samples/brightcove-player-sample-back-forward-buttons/back-button.png'
  );
  utilities.backButtonImage.setAttribute('alt', 'Double arrows pointing left');
  utilities.backButtonImage.setAttribute('width', '100');
  utilities.backButtonImage.setAttribute('height', '80');

  // Build the back button
  utilities.backButton.classList.add('brightcove-back-button');
  utilities.backButton.setAttribute('type', 'button');
  utilities.backButton.setAttribute(
    'title',
    'Skip back ' + utilities.jumpAmount + ' seconds'
  );
  utilities.backButton.setAttribute('aria-disabled', 'false');
  // Add the back button image into the back button
  utilities.backButton.appendChild(utilities.backButtonImage);

  // Build the forward button Image
  utilities.forwardButtonImage.setAttribute(
    'src',
    'https://player.support.brightcove.com/assets/images/code-samples/brightcove-player-sample-back-forward-buttons/forward-button.png'
  );
  utilities.forwardButtonImage.setAttribute(
    'alt',
    'Double arrows pointing right'
  );
  utilities.forwardButtonImage.setAttribute('width', '100');
  utilities.forwardButtonImage.setAttribute('height', '80');

  // Build the forward button
  utilities.forwardButton.classList.add('brightcove-forward-button');
  utilities.forwardButton.setAttribute('type', 'button');
  utilities.forwardButton.setAttribute(
    'title',
    'Skip forward ' + utilities.jumpAmount + ' seconds'
  );
  utilities.forwardButton.setAttribute('aria-disabled', 'false');

  // Add the forward button image into the forward button
  utilities.forwardButton.appendChild(utilities.forwardButtonImage);

  // insertBeforeNode = utilities.player.$('.vjs-volume-panel');
  utilities.controlBar.insertBefore(
    utilities.backButton,
    utilities.insertBeforeNode
  );
  utilities.controlBar.insertBefore(
    utilities.forwardButton,
    utilities.insertBeforeNode
  );

  utilities.backButton.addEventListener('click', function () {
    var newTime;
    var videoTime = player.currentTime();

    if (videoTime >= utilities.jumpAmount) {
      newTime = videoTime - utilities.jumpAmount;
    } else {
      newTime = 0;
    }
    player.currentTime(newTime);
  });

  utilities.forwardButton.addEventListener('click', function () {
    var newTime;
    var videoTime = player.currentTime();
    var videoDuration = player.duration();

    if (videoTime + utilities.jumpAmount <= videoDuration) {
      newTime = videoTime + utilities.jumpAmount;
    } else {
      newTime = videoDuration;
    }
    player.currentTime(newTime);
  });
});
