videojs.addLanguage("fr", {
  "Captions Settings": "Paramètres des Sous-titres",
  "Subtitles Settings": "Paramètres des Sous-titres",
  "Captions Off": "Sous-titres Désactivés",
  "Subtitles Off": "Sous-titres Désactivés",
  "Captions/Subtitles Off": "Sous-titres Désactivés",
  "Captions/Subtitles Settings": "Paramètres des Sous-titres"
});

videojs.addLanguage("de", {
  "Captions Settings": "Untertiteleinstellungen",
  "Subtitles Settings": "Untertiteleinstellungen",
  "Captions Off": "Untertitel Aus",
  "Subtitles Off": "Untertitel Aus",
  "Captions/Subtitles Off": "Untertitel Aus",
  "Captions/Subtitles Settings": "Untertiteleinstellungen"
});

videojs.addLanguage("ja", {
  "Captions Settings": "キャプションの設定",
  "Subtitles Settings": "字幕の設定",
  "Captions Off": "キャプションオフ",
  "Subtitles Off": "字幕オフ",
  "Captions/Subtitles Off": "キャプション/字幕オフ",
  "Captions/Subtitles Settings": "キャプション/字幕の設定"
});

videojs.addLanguage("es", {
  "Captions Settings": "Configuración de Subtítulos",
  "Subtitles Settings": "Configuración de Subtítulos",
  "Captions Off": "Subtítulos Desactivados",
  "Subtitles Off": "Subtítulos Fuera",
  "Captions/Subtitles Off": "Subtítulos Desactivados",
  "Captions/Subtitles Settings": "Configuración de Subtítulos"
});

videojs.addLanguage("it", {
  "Captions Settings": "Impostazioni Didascalie",
  "Subtitles Settings": "Impostazioni Sottotitoli",
  "Captions Off": "Didascalie Disattivate",
  "Subtitles Off": "Sottotitoli Disattivati",
  "Captions/Subtitles Off": "Didascalie/Sottotitoli Disattivati",
  "Captions/Subtitles Settings": "Impostazioni Didascalie/Sottotitoli"
});

videojs.addLanguage("pt-BR", {
  "Captions Settings": "Configuração de CC",
  "Subtitles Settings": "Configuração de Legendas",
  "Captions Off": "CC Desativada",
  "Subtitles Off": "Legendas Desativadas",
  "Captions/Subtitles Off": "CC/Legendas Desativadas",
  "Captions/Subtitles Settings": "Configuração de CC/Legendas"
});
