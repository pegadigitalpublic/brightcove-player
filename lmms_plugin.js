videojs.registerPlugin('lmms', function (options) {
  // Prevent initialize the plugin multiple times.
  // The plugin is enabled from brightcove-init and conditionally from
  // pega-analytics (as a fallback).
  if (typeof this.activePlugins_ !== 'undefined' &&
    typeof this.activePlugins_['lmms'] !== 'undefined' &&
    this.activePlugins_['lmms'] === true
  ) {
    return;
  }

  let adStateRegex,
    dataSetupOptions,
    defaultsEventsToTrack,
    eventsToTrack,
    getClosestAttribute,
    getFormattedDate,
    getParameterByName,
    isInAdState,
    lmmsEndpoint,
    parsedOptions,
    play,
    player,
    playerEl,
    isBackgroundVideo,
    sendbeacon,
    trackEvent;

  let __indexOf =
    [].indexOf ||
    function (item) {
      for (var i = 0, l = this.length; i < l; i++) {
        if (i in this && this[i] === item) return i;
      }
      return -1;
    };

  if (options == null) {
    options = {};
  }
  player = this;

  // Get player's HTML node.
  playerEl = player.el_;

  // Check if video is added as background video.
  isBackgroundVideo = (playerEl.closest('bolt-background') || playerEl.closest('.c-bolt-background')) !== null;

  dataSetupOptions = {};
  if (this.options()['data-setup']) {
    parsedOptions = JSON.parse(this.options()['data-setup']);
    if (parsedOptions.ga) {
      dataSetupOptions = parsedOptions.ga;
    }
  }
  defaultsEventsToTrack = [
    // 'player_load',
    // 'video_load',
    // 'percent_played',
    // 'start',
    // 'end',
    // 'seek',
    'play',
    // 'pause',
    // 'resize',
    // 'volume_change',
    // 'fullscreen',
    // 'error',
  ];
  eventsToTrack =
    options.eventsToTrack ||
    dataSetupOptions.eventsToTrack ||
    defaultsEventsToTrack;
  adStateRegex = /(\s|^)vjs-ad-(playing|loading)(\s|$)/;
  isInAdState = function (player) {
    return adStateRegex.test(player.el().className);
  };

  if (typeof options != 'undefined' &&
    typeof options.lmmsEndpoint != 'undefined'
  ) {
    lmmsEndpoint = options.lmmsEndpoint;
  }
  else if (typeof window.drupalSettings != 'undefined' &&
    typeof window.drupalSettings.lmms != 'undefined'
  ) {
    lmmsEndpoint = window.drupalSettings.lmms.path;
  }

  if (lmmsEndpoint) {
    lmmsEndpoint += '/CapturePageVisit';
    if (!lmmsEndpoint.startsWith('http')) {
      if (!lmmsEndpoint.startsWith('/')) {
        lmmsEndpoint = '/' + lmmsEndpoint;
      }
      lmmsEndpoint =
        window.location.protocol +
        '//' +
        window.location.hostname +
        lmmsEndpoint;
    }
  }

  /**
   * Handle media (audio / video) play event
   */
  play = function () {
    if (!isInAdState(player)) {
      trackEvent({
        evt: 'Play',
        continuousTrace: false,
      });
    }
  };

  /**
   * Get formatted date.
   *
   * @param date
   * @returns {string}
   */
  getFormattedDate = function (date) {
    let yyyy = date.getUTCFullYear(),
      mm = date.getUTCMonth() + 1,
      dd = date.getUTCDate(),
      hh = date.getUTCHours(),
      min = date.getUTCMinutes(),
      ss = date.getUTCSeconds();
    if (mm < 10) mm = '0' + mm;
    if (dd < 10) dd = '0' + dd;
    if (hh < 10) hh = '0' + hh;
    if (min < 10) min = '0' + min;
    if (ss < 10) ss = '0' + ss;

    return String(yyyy) + mm + dd + 'T' + hh + min + ss + '.000 GMT';
  };

  /**
   * Get param value from current url query string.
   *
   * @param name
   *   Param name.
   *
   * @returns {string|null}
   */
  getParameterByName = function (name) {
    let url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    let regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
      results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';

    return decodeURIComponent(results[2].replace(/\+/g, ' '));
  };

  /**
   * Get closest element for given.
   *
   * @param elem
   * @param selector
   * @param attribute
   * @returns {string}
   */
  getClosestAttribute = function (elem, selector, attribute) {
    // Element.matches() polyfill
    if (!Element.prototype.matches) {
      Element.prototype.matches =
        Element.prototype.matchesSelector ||
        Element.prototype.mozMatchesSelector ||
        Element.prototype.msMatchesSelector ||
        Element.prototype.oMatchesSelector ||
        Element.prototype.webkitMatchesSelector ||
        function (s) {
          let matches = (this.document || this.ownerDocument).querySelectorAll(
              s
            ),
            i = matches.length;
          while (--i >= 0 && matches.item(i) !== this) {}
          return i > -1;
        };
    }

    // Get the closest matching element
    for (; elem && elem !== document; elem = elem.parentNode) {
      if (elem.matches(selector)) {
        if (elem.hasAttribute(attribute)) {
          return elem.getAttribute(attribute);
        }
      }
    }

    return '';
  };

  /**
   * Track Event
   *
   * @param opts
   *    Set LMMS event name:
   *      {
   *        evt: 'Play'
   *      }
   *
   *    Enable continuous tracking:
   *      {
   *        continuousTrace: true
   *      }
   */
  trackEvent = function (opts) {
    if (typeof lmms == 'undefined' || typeof opts == 'undefined' || !opts.evt) {
      return;
    }

    let tracedEventName = 'lmmsEvent' + opts.evt;
    let continuousTrace =
      typeof opts.continuousTrace !== 'undefined'
        ? !!opts.continuousTrace
        : true;

    // Don't send the same event for a next time.
    if (
      !continuousTrace &&
      typeof player.el_[tracedEventName] !== 'undefined' &&
      player.el_[tracedEventName] === true
    ) {
      return;
    }

    /**
     * Register LMMS Callback function called after ajax success.
     *
     * Mark player that the event was tracked.
     */
    lmms.continuousTraceCallback = function () {
      if (!continuousTrace) {
        player.el_[tracedEventName] = true;
      }
    };

    let data = {
      Activity: {
        ASSET_ID: player.mediainfo.id,
        ASSET_NAME: player.el_.getAttribute('data-pega-cd-id') || '',
        ASSETDISPLAYNAME: player.mediainfo.name,
        ASSET_TYPE:
          player.el_.getAttribute('data-pega-video-asset-type') || 'Video',
        ACTIVITY_TYPE:
          player.el_.getAttribute('data-pega-video-type') || 'Video',
        ACTIVITY_SUB_TYPE: opts.evt,
        URL: window.location.href,
        UTM_SOURCE: getParameterByName('utm_source') || '',
        UTM_KEYWORD:
          getParameterByName('utm_keyword') ||
          getParameterByName('utm_term') ||
          '',
        UTM_CAMPAIGN: getParameterByName('utm_campaign') || '',
        UTM_MEDIUM: getParameterByName('utm_medium') || '',
        UTM_CONTENT: getParameterByName('utm_content') || '',
        INTERACTION_ID:
          getClosestAttribute(
            player.el_,
            '[data-cdh-interaction-id]',
            'data-cdh-interaction-id'
          ) ||
          getParameterByName('IxID') ||
          '',
        PARENT_OFFER_ID:
          getClosestAttribute(
            player.el_,
            '[data-cdh-parent-offer-id]',
            'data-cdh-parent-offer-id'
          ) ||
          getParameterByName('PO') ||
          '',
        OFFER_ID:
          getClosestAttribute(
            player.el_,
            '[data-cdh-offer-id]',
            'data-cdh-offer-id'
          ) ||
          getParameterByName('O') ||
          '',
        LENGTH: Math.round(player.duration() / 60),
        EVENT_DATE: getFormattedDate(new Date()),
      },
    };

    // Add taxonomy terms if LMMS version supports that method.
    if (
      typeof lmms.getPageTermsFromMetatags === 'function' &&
      typeof lmms.getLmmsTermsFromPageTerms === 'function'
    ) {
      let terms = lmms.getPageTermsFromMetatags();
      let mapped_terms = lmms.getLmmsTermsFromPageTerms(terms);
      for (let vocab in mapped_terms) {
        data.Activity[vocab] = mapped_terms[vocab];
      }
    }

    sendbeacon(data, 'continuousTraceCallback');
  };

  /**
   * Send event data to event service.
   *
   * @param requestObject
   *   LMMS Request Parameters object.
   * @param callbackFunctionName
   *   Callback function name being called on ajaxSuccess.
   */
  sendbeacon = function (requestObject, callbackFunctionName) {
    // Don't track video background.
    if (isBackgroundVideo) {
      return;
    }

    if (typeof lmms !== 'undefined' && typeof lmmsEndpoint !== 'undefined') {
      // Use LMMS build in ajax call.
      if (typeof lmms.makeAjaxCall === 'function') {
        lmms.makeAjaxCall(lmmsEndpoint, requestObject, callbackFunctionName);
      } else {
        // Use vanilla JS ajax call.
        let xhr = new XMLHttpRequest();
        xhr.open('POST', lmmsEndpoint, true);
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.withCredentials = true;
        xhr.onreadystatechange = function () {
          if (xhr.readyState === 4 && xhr.status === 200) {
            if (typeof lmms[callbackFunctionName] === 'function') {
              lmms[callbackFunctionName]();
            }
            // Show status information when debug is on.
            if (options.debug) {
              let json = JSON.parse(xhr.responseText);
              videojs.log(json.Status);
            }
          }
        };
        xhr.send(JSON.stringify(requestObject));
      }
    } else if (options.debug) {
      videojs.log('LMMS tracking library not detected.');
    }
  };

  /*
   * Events attach.
   */

  this.ready(function () {
    this.on('loadedmetadata', function () {
      // For delayed Video play event handler must be added when metatata are
      // loaded. Otherwise, the play event will be called too early and won't
      // contain  all required metadata, eg duration.
      if (__indexOf.call(eventsToTrack, 'play') >= 0) {
        this.on('play', play);
      }
    });

    // Fix concurrency issue, where `play` event is dispatched before
    // `loadedmetadata` event adds it.
    if (__indexOf.call(eventsToTrack, 'play') >= 0) {
      this.on('play', play);
    }
  });
});
