videojs.registerPlugin('ga4', function (options) {
  let
    adStateRegex,
    currentVideo,
    dataSetupOptions,
    defaultsEventsToTrack,
    defaultLabel,
    delayed,
    end,
    endTracked,
    eventLabel,
    eventsToTrack,
    getFormattedError,
    isInAdState,
    loaded,
    mediaId,
    mediaTitle,
    mediaDuration,
    mediaEcmId,
    mediaEcmLanguage,
    pause,
    paused,
    percentsAlreadyTracked,
    percentsPlayedInterval,
    play,
    player,
    playerEl,
    playerMode,
    isBackgroundVideo,
    referrer,
    seekEnd,
    seeking,
    seekStart,
    sendbeacon,
    startTracked,
    timeupdate;

  let __indexOf =
    [].indexOf ||
    function (item) {
      for (let i = 0, l = this.length; i < l; i++) {
        if (i in this && this[i] === item) return i;
      }
      return -1;
    };

  if (options == null) {
    options = {};
  }
  referrer = document.createElement('a');
  referrer.href = document.referrer;
  if (
    self !== top &&
    window.location.host === 'preview-players.brightcove.net' &&
    referrer.hostname === 'studio.brightcove.com'
  ) {
    videojs.log(
      'Google analytics plugin will not track events in Video Cloud Studio'
    );
    return;
  }

  // Store player reference.
  player = this;

  // Get player's HTML node.
  playerEl = player.el_;

  // Get media type.
  // 'audio' and 'video' are allowed. Video is default option.
  playerMode = player.el_.classList.contains('c-base-video--audio-only') ? 'audio' : 'video';

  // Check if video is added as background video.
  isBackgroundVideo = (playerEl.closest('bolt-background') || playerEl.closest('.c-bolt-background')) !== null;

  // Get Brightcove media ID.
  mediaId = player.mediainfo && player.mediainfo.id ? player.mediainfo.id : '';

  // Get ECM Content ID.
  mediaEcmId = playerEl.dataset.pegaCdId || 'undefined';

  // Get ECM Content / Video Language.
  // This is expected that translation name is english full name.
  mediaEcmLanguage = playerEl.dataset.pegaCdTranslation || 'undefined';

  // Check if video is played inline or in modal.
  delayed = function() {
    return (this.el_.dataset.videoDelayed === '' || this.el_.dataset.videoDelayed === 'true' || this.el_.dataset.delayedPlayer);
  }.bind(this);

  /**
   * Get media label.
   *
   * @type string
   */
  mediaTitle = function() {
    if (defaultLabel) {
      eventLabel = defaultLabel;
    } else {
      if (this.mediainfo && this.mediainfo.name) {
        eventLabel = this.mediainfo.name;
      } else {
        eventLabel = this.currentSrc()
          .split('/')
          .slice(-1)[0]
          .replace(/\.(\w{3,4})(\?.*)?$/i, '');
      }
    }
    return eventLabel;
  }.bind(this);

  /**
   * Get media duration.
   *
   * @type int
   */
  mediaDuration = function() {
    return Math.floor(this.duration());
  }.bind(this);

  dataSetupOptions = {};
  if (this.options()['data-setup-ga4']) {
    let parsedOptions = JSON.parse(this.options()['data-setup-ga4']);
    if (parsedOptions.ga4) {
      dataSetupOptions = parsedOptions.ga4;
    }
  }
  defaultsEventsToTrack = [
    // 'player_load',
    'video_load',
    'percent_played',
    // 'start',
    'end',
    'seek',
    'play',
    'pause',
    // 'resize',
    // 'volume_change',
    // 'fullscreen',
    // 'error',
  ];
  eventsToTrack =
    options.eventsToTrack ||
    dataSetupOptions.eventsToTrack ||
    defaultsEventsToTrack;
  percentsPlayedInterval =
    options.percentsPlayedInterval ||
    dataSetupOptions.percentsPlayedInterval ||
    25;
  percentsAlreadyTracked = [];
  startTracked = false;
  endTracked = false;
  seekStart = seekEnd = 0;
  seeking = false;
  paused = false;
  eventLabel = '';
  currentVideo = '';
  defaultLabel = options.eventLabel || dataSetupOptions.eventLabel;
  adStateRegex = /(\s|^)vjs-ad-(playing|loading)(\s|$)/;
  isInAdState = function (player) {
    return adStateRegex.test(player.el().className);
  };

  /*
   * Events to be tracked.
   */

  /**
   * Handle media (audio / video) load event.
   */
  loaded = function () {
    if (!isInAdState(player)) {
      if (
        player.mediainfo &&
        player.mediainfo.id &&
        player.mediainfo.id !== currentVideo
      ) {
        currentVideo = player.mediainfo.id;
        percentsAlreadyTracked = [];
        startTracked = false;
        endTracked = false;
        seekStart = seekEnd = 0;
        seeking = false;
        if (__indexOf.call(eventsToTrack, 'video_load') >= 0) {
          // Send video_loaded / audio_loaded tracking events.
          sendbeacon(
            '{media-type}_loaded',
            {
              '{media-type}_id': mediaId,
              '{media-type}_title': mediaTitle(),
              '{media-type}_duration': mediaDuration(),
              ecm_id: mediaEcmId,
              ecm_language: mediaEcmLanguage
            }
          );
        }

        if (delayed() && __indexOf.call(eventsToTrack, 'play') >= 0) {
          this.trigger('play');
        }
      }
    }
  };

  /**
   * Handle media (audio / video) time update event.
   * The function sends ga4 event on 25 / 50 / 75 / 100 percent view.
   */
  timeupdate = function () {
    let currentTime, duration, percent, percentPlayed, _i;
    if (!isInAdState(player)) {
      currentTime = Math.floor(this.currentTime());
      duration = Math.floor(this.duration());
      percentPlayed = Math.round((currentTime / duration) * 100);
      for (
        percent = _i = 0;
        _i <= 100;
        percent = _i += percentsPlayedInterval
      ) {
        if (
          percentPlayed >= percent &&
          __indexOf.call(percentsAlreadyTracked, percent) < 0
        ) {
          if (
            __indexOf.call(eventsToTrack, 'percent_played') >= 0 &&
            percentPlayed !== 0 &&
            percent !== 0
          ) {
            // Send video_progress / audio_progress tracking events.
            sendbeacon(
              '{media-type}_progress',
              {
                '{media-type}_id': mediaId,
                '{media-type}_title': mediaTitle(),
                '{media-type}_duration': mediaDuration(),
                '{media-type}_current_time': currentTime,
                '{media-type}_percent': percent,
                ecm_id: mediaEcmId,
                ecm_language: mediaEcmLanguage
              }
            );
          }
          if (percentPlayed > 0) {
            percentsAlreadyTracked.push(percent);
          }
        }
      }

      if (__indexOf.call(eventsToTrack, 'seek') >= 0) {
        seekStart = seekEnd;
        seekEnd = currentTime;

        if (Math.abs(seekStart - seekEnd) > 1) {
          // Send video_seek / audio_seek tracking events.
          sendbeacon(
            '{media-type}_seek',
            {
              '{media-type}_id': mediaId,
              '{media-type}_title': mediaTitle(),
              '{media-type}_duration': mediaDuration(),
              '{media-type}_current_time': currentTime,
              ecm_id: mediaEcmId,
              ecm_language: mediaEcmLanguage
            }
          );

          seeking = true;
        }
      }
    }
  };

  /**
   * Handle media (audio / video) play event
   */
  play = function () {
    let currentTime;
    if (!isInAdState(player)) {
      currentTime = Math.floor(this.currentTime());
      if (paused) {
        // Send video_resume / audio_resume tracking events.
        sendbeacon(
          '{media-type}_resume',
          {
            '{media-type}_id': mediaId,
            '{media-type}_title': mediaTitle(),
            '{media-type}_duration': mediaDuration(),
            '{media-type}_current_time': currentTime,
            ecm_id: mediaEcmId,
            ecm_language: mediaEcmLanguage
          }
        );
      } else {
        // Send video_play / audio_play tracking events.
        sendbeacon(
          '{media-type}_play',
          {
            '{media-type}_id': mediaId,
            '{media-type}_title': mediaTitle(),
            '{media-type}_duration': mediaDuration(),
            ecm_id: mediaEcmId,
            ecm_language: mediaEcmLanguage
          }
        );
      }
      paused = false;
      seeking = false;
    }
  };

  /**
   * Handle media (audio / video) pause event.
   */
  pause = function () {
    let currentTime, duration;
    paused = true;
    if (!isInAdState(player)) {
      currentTime = Math.floor(this.currentTime());
      duration = Math.floor(this.duration());
      if (currentTime !== duration && !seeking) {
        // Send video_pause / audio_pause tracking events.
        sendbeacon(
          '{media-type}_pause',
          {
            '{media-type}_id': mediaId,
            '{media-type}_title': mediaTitle(),
            '{media-type}_duration': mediaDuration(),
            '{media-type}_current_time': currentTime,
            ecm_id: mediaEcmId,
            ecm_language: mediaEcmLanguage
          }
        );
      }
    }
  };

  /**
   * Handle media (audio / video) completed event.
   */
  end = function () {
    if (!isInAdState(player)) {
      // Send video_complete / audio_complete tracking events.
      sendbeacon(
        '{media-type}_complete',
        {
          '{media-type}_id': mediaId,
          '{media-type}_title': mediaTitle(),
          '{media-type}_duration': mediaDuration(),
          ecm_id: mediaEcmId,
          ecm_language: mediaEcmLanguage
        }
      );
      seekStart = seekEnd = 0;
      paused = false;
    }
  };

  /*
   * GA4 requests.
   */

  /**
   * Send event data to event service.
   *
   * @param eventName
   *   Event Name.
   * @param parameters
   *   Event Parameters object.
   */
  sendbeacon = function (
    eventName,
    parameters
  ) {
    // Don't track video background.
    if (isBackgroundVideo) {
      return;
    }

    if (typeof Drupal !== 'undefined') {
      if (Drupal && Drupal.pegaAnalytics && Drupal.pegaAnalytics.ga4SendEvent) {
        let eventParams = {};
        let _eventName = eventName.replace('{media-type}', playerMode);

        for (let paramName in parameters) {
          let parameter = parameters[paramName];
          let _paramName = paramName.replace('{media-type}', playerMode);

          eventParams[_paramName] = parameter;
        }

        Drupal.pegaAnalytics.ga4SendEvent(_eventName, eventParams);
      }
    } else if (options.debug) {
      videojs.log('Google Analytics 4 not detected');
    }
  };

  /**
   * Get formatted error for sendbeacon format.
   *
   * @param playbackResponse
   *   Brightcove playback response.
   *
   * @returns {[string,{video_duration, video_title, ecm_id, ecm_language, video_id}]}
   */
  getFormattedError = function(playbackResponse) {
    let currentTime = Math.floor(player.currentTime());
    // Send video_error / audio_error tracking events.
    return [
      '{media-type}_error',
      {
        '{media-type}_id': mediaId,
        '{media-type}_title': mediaTitle(),
        '{media-type}_duration': mediaDuration(),
        '{media-type}_current_time': currentTime,
        ecm_id: mediaEcmId,
        ecm_language: mediaEcmLanguage
      }
    ]
  }

  /*
   * Events attach.
   */

  this.ready(function() {
    let href, iframe;

    this.on('loadedmetadata', function() {
      // For delayed Video play event handler must be added when metatata are
      // loaded. Otherwise, the play event will be called too early and won't
      // contain  all required metadata, eg duration.
      if (__indexOf.call(eventsToTrack, 'play') >= 0) {
        this.on('play', play);
      }
    });

    this.on('loadedmetadata', loaded);
    this.on('timeupdate', timeupdate);

    if (__indexOf.call(eventsToTrack, 'pause') >= 0) {
      this.on('pause', pause);
    }

    if (__indexOf.call(eventsToTrack, 'end') >= 0) {
      this.on('ended', end);
    }
  });

  return {
    sendbeacon: sendbeacon,
    getFormattedError: getFormattedError
  };
});
