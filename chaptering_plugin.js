videojs.registerPlugin('chaptering', function () {
  var player = this;

  var videoDuration = 0;
  var cuesAra = [];
  var chapterStartTimesAra = [];

  var addMarkers = function (cuePointsAra, videoDuration) {
    var iMax = cuePointsAra.length,
      i,
      playheadWell = document.getElementsByClassName(
        'vjs-progress-control vjs-control'
      )[0];
    // Loop over each cue point, dynamically create a div for each
    // then place in div progress bar
    for (i = 0; i < iMax; i++) {
      var elem = document.createElement('div');
      elem.className = 'vjs-marker';
      elem.id = 'cp' + i;
      elem.style.left = (cuePointsAra[i] / videoDuration) * 100 + '%';
      console.log('elem.style.left', elem.style.left);
      playheadWell.appendChild(elem);
    }
  };

  var chapterTT = [].filter.call(player.textTracks(), function (tt) {
    //console.log(tt.kind);
    return tt.kind === 'chapters';
  });
  console.log(player.textTracks()[0].cues);
  //console.log(chapterTT[0]);
  if (chapterTT[0]) {
    //  Retrieve actual array of chapter cue points
    cuesAra = chapterTT[0].cues;
    //console.log(cuesAra);
    // +++ Loop over chapter cue points and get start time of each  +++
    for (var i = 0; i < cuesAra.length; i++) {
      chapterStartTimesAra[i] = cuesAra[i].startTime;
    }

    // +++ Call function to create marks in progress bar  +++
    // Get the video duration
    videoDuration = player.mediainfo.duration;

    // Call the function to add the marks in the progress control
    addMarkers(chapterStartTimesAra, videoDuration);
  }
});
