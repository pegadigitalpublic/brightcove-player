// Dev Notes
// 1. <bolt-modal> will soon be deprecated. Use <dialog class="c-bolt-dialog"> instead.
//    Once <bolt-modal> is removed, update "modal" references to "dialog" to avoid confusion.
//    Also, rename "show" => "open", "hide" => "close".

var players = document.querySelectorAll('.c-base-video');
// EXTERNAL CONTROLS
// Button requires a "js-base-video-toggle" class (how the functionality will be hooked up)
// Button requires "data-video-target" attribute (this is how the video is targeted on click)
var externalControlButtons = document.querySelectorAll('.js-base-video-toggle');
const motionMediaQuery = window.matchMedia('(prefers-reduced-motion: reduce)');
const prefersReducedMotion = !motionMediaQuery || motionMediaQuery.matches;

var handleExternalButtonClick = (e) => {
  e.preventDefault();
  var videoTargetPlayer = videojs.getPlayer(
    e.currentTarget.dataset.videoTarget
  );
  if (videoTargetPlayer) {
    var isVideoThumbnail = videoTargetPlayer.el_.closest(
      '.c-bolt-video-thumbnail'
    );
    var isPlaying = videoTargetPlayer.el_.classList.contains('vjs-playing');

    if (isVideoThumbnail) {
      var playButton = isVideoThumbnail.querySelector(
        '.js-bolt-video-thumbnail-inline-button'
      );

      if (playButton) {
        playButton.dispatchEvent(new Event('click'));
      }
    }

    if (isPlaying) {
      videoTargetPlayer.pause();
    } else {
      videoTargetPlayer.play();
    }
  }
};

for (var eb = 0; eb < externalControlButtons.length; eb++) {
  externalControlButtons[eb].addEventListener(
    'click',
    handleExternalButtonClick
  );
}

let runParamsOnce = false;

var videoInit = (thisPlayer) => {
  thisPlayer.on('error', function (err) {
    if (this.ga4) {
      let errorTrackers = [];
      errorTrackers.push({
        sendbeacon: this.ga().sendbeacon,
        getFormattedError: this.ga().getFormattedError,
      });
      errorTrackers.push({
        sendbeacon: this.ga4().sendbeacon,
        getFormattedError: this.ga4().getFormattedError,
      });
      this.errorReporting(this, err, errorTrackers);
    } else {
      // Backward compatibility support.
      this.errorReporting(this, err, this.ga().sendbeacon);
    }
    // OPTIONS
    // data-custom-error-message (optional) [string]: Pass in a acustom message to be displayed on errors
    this.customError(this.getAttribute('data-custom-error-message'));
  });
  thisPlayer.ready(function () {
    if (
      this.el_.hasAttribute('data-start-at') ||
      this.el_.hasAttribute('data-stop-at')
    ) {
      if (typeof this.clip === 'function') {
        this.clip();
      }
    }
    // OPTIONS
    // data-playback-rates (optional) [array]: Sets the playback rates on the player controls
    if (this.el().dataset.playbackRates) {
      var playbackRateOptions = this.getAttribute(
        'data-playback-rates'
      ).replace(/(\[*\]*)/g, '');
      playbackRateOptions = playbackRateOptions.split(',');
      if (typeof thisPlayer.playbackRates === 'function') {
        var playbackRateOptionsClean = [];
        for (var o = 0; o < playbackRateOptions.length; o++) {
          playbackRateOptionsClean.push(parseFloat(playbackRateOptions[o], 10));
        }
        thisPlayer.playbackRates(playbackRateOptionsClean);
      } else {
        thisPlayer.controlBar.playbackRateMenuButton =
          thisPlayer.controlBar.addChild(
            'PlaybackRateMenuButton',
            {
              playbackRates: playbackRateOptions,
            },
            10
          );
      }
    }
    // OPTIONS
    // data-default-resolution (optional) [string]: Adjusts the default resolution for the videos (ex. 720p)
    qualityOptions = {
      useResolutionLabels: true,
      defaultResolution: this.getAttribute('data-default-resolution') || '720p',
    };
    this.qualityMenu(qualityOptions);
    // OPTIONS
    // data-social-title (optional) [string]: CURRENT NOT BEING DISPLAYED
    // data-social-description (optional) [string]: Social Panel title
    // data-social-url (optional[default: the page url]) [string]: change the shared social url
    // data-social-facebook (optional [default: true]) [boolean]: Displays the Facebook button
    // data-social-x (optional [default: true]) [boolean]: Displays the X button
    // data-social-tumblr (optional [default: false]) [boolean]: Displays the tumblr button
    // data-social-pinterest (optional [default: false]) [boolean]: Displays the Pinterest button
    // data-social-linkedin (optional [default: true]) [boolean]: Displays the Linkedin button
    socialOptions = {
      title: this.getAttribute('data-social-title') || 'Share This Video',
      description: 'Share This Video',
      url: this.getAttribute('data-social-url') || window.location.href,
      removeEmbed: true,
      services: {
        facebook: this.el_.hasAttribute('data-social-facebook') ? true : false,
        x: this.el_.hasAttribute('data-social-x') ? true : false,
        tumblr: this.el_.hasAttribute('data-social-tumblr') ? true : false,
        pinterest: this.el_.hasAttribute('data-social-pinterest')
          ? true
          : false,
        linkedin: this.el_.hasAttribute('data-social-linkedin') ? true : false,
      },
      displayAfterVideo: this.el_.classList.contains('vjs-controls-disabled')
        ? false
        : true,
      buttonParent: 'controlBar',
    };
    this.social(socialOptions);
    if (typeof this.ga === 'function') {
      this.ga();
    }
    if (typeof this.ga4 === 'function') {
      this.ga4();
    }
    if (typeof this.lmms === 'function') {
      this.lmms();
    }
    // OPTIONS
    // data-social-email (required) [boolean]: option to display the email icon
    // data-email-videoid (required) [string]: Video ID
    // data-email-videotitle (required) [string]: Video email title
    // data-email-body (required) [string]: Video email body
    // data-email-subject (required) [string]: Video email subject line
    // data-email-videourl (optional) [string]: Video URL to share in email
    if (this.el_.classList.contains('vjs-controls-disabled')) {
      this.emailSocialShare(false);
    } else {
      this.emailSocialShare(true);
    }
    // Move the fullscreen button to the far right of the controlbar
    if (thisPlayer.controlBar.getChild('FullscreenToggle')) {
      var fullScreenButton =
        thisPlayer.controlBar.getChild('FullscreenToggle').el_;
      thisPlayer.controlBar.el_.appendChild(fullScreenButton);
    }
    if (this.el_.classList.contains('c-base-video--audio-only')) {
      this.arrowNavigation();
    }
  });
  thisPlayer.on('loadstart', function (player) {
    // OPTIONS
    // data-media-title (optional [default: true]) [boolean]: hide the title
    // data-media-duration (optional [default: true]) [boolean]: hide the duration
    // data-media-preview (optional [default: false]) [boolean]: show the preview label
    this.metaData(
      this.el_.hasAttribute('data-media-title') ? this.mediainfo.name : '',
      this.el_.hasAttribute('data-media-duration')
        ? this.mediainfo.duration
        : '',
      this,
      this.el_.hasAttribute('data-media-preview')
    );
  });

  thisPlayer.on('loadedmetadata', function (player) {
    if (this.el_.hasAttribute('autoplay')) {
      if (prefersReducedMotion) {
        thisPlayer.pause();
      } else {
        thisPlayer.play();
      }
    }

    // PLAY VIDEO FROM URL
    // Use the following URL query format "?videoid=6092957725001&timestamp=200"
    // "videoid": the video ID of a brightcove video, this will be the target video
    // "timestamp": the place where the video should start from (in seconds)
    const params = new Proxy(new URLSearchParams(window.location.search), {
      get: (searchParams, prop) => searchParams.get(prop),
    });

    if (params.videoid && !runParamsOnce) {
      this.timeStampQuery(params);
      runParamsOnce = true;
    }

    //OPTIONS
    // data-autoplay-on-viewport (optional [default: false]) [boolean]: will play the video when it comes into view
    // REQUIRES the "muted" attribute to be placed on the video-js
    if (this.el_.hasAttribute('data-autoplay-on-viewport')) {
      // check against media query, if user prefers reduced motion do not autoplay video.
      if (!prefersReducedMotion) {
        this.autoplayOnViewport();
      }
    }
  });
  thisPlayer.on('ended', function (player) {
    // change icon of external button to play when video ends
    const relatedButton = document.querySelector(
      '[data-video-target="' + thisPlayer.id_ + '"]'
    );

    // confirm button exists, make sure button and video share parent layout element.
    if (
      relatedButton &&
      thisPlayer.el_.closest('bolt-layout') ===
        relatedButton.closest('bolt-layout')
    ) {
      const pause =
        'M11,10 L18,13.74 18,22.28 11,26 M18,13.74 L26,18 26,18 18,22.28';
      const play = 'M11,10 L17,10 17,26 11,26 M20,10 L26,10 26,26 20,26';
      const animationTarget = relatedButton.querySelector(
        '.background-video-toggle-animation'
      );

      relatedButton.setAttribute('aria-label', 'Play the background video');
      animationTarget.setAttribute('from', play);
      animationTarget.setAttribute('to', pause);
      relatedButton.classList.remove('is-playing');
      animationTarget.beginElement();
    }
  });
  thisPlayer.one('play', function (player) {
    // NOTE: Safari does not immediately load the closed captioning menu on load, need to first wait until the first play event + 1/2 second wait time to avoid race conditions
    window.setTimeout(
      function () {
        this.captionSubtitle();
        this.closeCaptioning();
      }.bind(this),
      500
    );
  });
};

players.forEach(function (player) {
  // Initialize the player if it hasn't already been.
  var playerId = player.getAttribute('data-player');
  if (
    !player.classList.contains('bc-player-' + playerId + '_default') &&
    !player.hasAttribute('data-video-delayed')
  ) {
    bc(player);
  }

  // [data-video-modal] prevents the video from being loaded on page load
  if (!player.hasAttribute('data-video-delayed')) {
    const myPlayer = videojs.getPlayer(player.id);
    // Initialize the Brigthcove video functionality
    videoInit(myPlayer);
    // If this video is in a modal, it should ideally be using "data-video-delayed".
    // However, there are cases where that's overkill, like when there's only one or two
    // such videos on a page and the video isn't in a place where 'is_delayed' can be easily/reliably
    // added (such as in an advanced modal).
    const modal = player.closest('bolt-modal') || player.closest('dialog'); // [1]
    const handleModalHide = (e) => {
      if (myPlayer) {
        myPlayer.pause();
      }
    };
    if (modal) {
      modal.addEventListener('modal:hide', handleModalHide); // [1]
      modal.addEventListener('dialog:close', handleModalHide);
    }
  } else {
    // Find all videos that have the "data-video-modal" data attribute
    const videoModals = document.querySelectorAll(
      'video-js[data-video-delayed]'
    );
    videoModals.forEach((video) => {
      // Find the closest dialog for each of the modal videos
      const modal = video.closest('bolt-modal') || video.closest('dialog'); // [1]
      if (modal) {
        let myPlayer;

        const handleModalShow = (e) => {
          if (video.hasAttribute('data-video-delayed')) {
            // Using the temporary data attribute ("data-modal..." attributes), create the Brightcove atttributes required for init
            video.setAttribute('data-account', video.dataset.delayedAccount);
            video.setAttribute('data-player', video.dataset.delayedPlayer);
            video.setAttribute('data-video-id', video.dataset.delayedVideoId);
            // Initialize thse Brightcove video
            myPlayer = bc(video);
            // Pass in the initialized video into the Pega custom videoInit function
            videoInit(myPlayer);
            //Remove the "data-video-modal"
            video.removeAttribute('data-video-delayed');
          }
          if (
            myPlayer &&
            !video.hasAttribute('data-video-delayed-no-autoplay')
          ) {
            if (!prefersReducedMotion) {
              myPlayer.play();
            }
          }
        };

        const handleModalHide = (e) => {
          if (myPlayer) {
            // Pause the video automatically
            myPlayer.pause();
          }
        };

        // Target the modal's "modal:show" event (as defined in 'packages/components/bolt-modal/src/modal.js')
        modal.addEventListener('modal:show', handleModalShow); // [1]
        modal.addEventListener('modal:hide', handleModalHide); // [1]

        // Target the dialog's "dialog:open" event (as defined in 'packages/components/bolt-dialog/src/dialog.js')
        modal.addEventListener('dialog:open', handleModalShow);
        modal.addEventListener('dialog:close', handleModalHide);
      }
    });
  }
});
