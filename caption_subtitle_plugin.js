videojs.registerPlugin('captionSubtitle', function () {
  let player = this;
  let showCaptions = false;
  let showSubtitles = false;

  // Localization
  const captionsLabel = player.localize('Captions');
  const subtitlesLabel = player.localize('Subtitles');
  const captionsSettingsLabel = player.localize('Captions Settings');
  const subtitlesSettingsLabel = player.localize('Subtitles Settings');
  const captionsOffLabel = player.localize('Captions Off');
  const subtitlesOffLabel = player.localize('Subtitles Off');
  const captionSubtitlesSettingsLabel = player.localize(
    'Captions/Subtitles Settings',
  );
  const captionSubtitlesOffLabel = player.localize('Captions/Subtitles Off');

  player.el_
    .querySelector('.vjs-subs-caps-button')
    .classList.add('vjs-subs-caps-button--mobile-show');

  const buildMenus = (buttonLabel, isCaption) => {
    let classLabel = '';
    const SubCapsButton = videojs.getComponent('SubsCapsButton');
    const subCapsButton = new SubCapsButton(player, { label: buttonLabel });
    if (isCaption) {
      classLabel = 'captions';
    } else {
      classLabel = 'subtitles';
    }
    subCapsButton.addClass('vjs-subs-caps-button--desktop-show');
    subCapsButton.addClass('vjs-subs-caps-button--' + classLabel);
    subCapsButton.controlText(buttonLabel);
    // Safari doesn't put the Caption/Subtitle controls in the menu so we need to use a conditional to prevent the JS from running
    const settingsControl = subCapsButton.el_.querySelector(
      '.vjs-texttrack-settings',
    );

    if (settingsControl) {
      if (isCaption) {
        settingsControl.querySelector('.vjs-menu-item-text').innerText =
          captionsSettingsLabel;
      } else {
        settingsControl.querySelector('.vjs-menu-item-text').innerText =
          subtitlesSettingsLabel;
      }
    }

    if (isCaption) {
      subCapsButton.el_.querySelector(
        '.vjs-menu-item:not(.vjs-subtitles-menu-item):not(.vjs-texttrack-settings)',
      ).innerText = captionsOffLabel;
    } else {
      subCapsButton.el_.querySelector(
        '.vjs-menu-item:not(.vjs-subtitles-menu-item):not(.vjs-texttrack-settings)',
      ).innerText = subtitlesOffLabel;
    }

    return subCapsButton.el_;
  };

  const applyMenu = (markup) => {
    const subCapsButton = player.el_.querySelector(
      '.vjs-control-bar .vjs-subs-caps-button',
    );
    subCapsButton.after(markup);
  };

  const removeListItems = (menuElement, showEnglish) => {
    const list = menuElement.querySelectorAll('.vjs-menu ul li');
    for (const listItem in list) {
      if (Object.hasOwnProperty.call(list, listItem)) {
        if (list[listItem].classList.contains('vjs-subtitles-menu-item')) {
          const listText = list[listItem].querySelector(
            '.vjs-menu-item-text',
          ).innerText;
          let conditional = '';
          if (showEnglish) {
            conditional = listText !== 'English';
          } else {
            conditional = listText === 'English';
          }
          if (conditional) {
            list[listItem].remove();
          }
        }
      }
    }
  };

  // conditional to prioritize data-attributes over text tracks to resolve potential race condition issues
  if (
    this.el_.hasAttribute('data-captions') ||
    this.el_.hasAttribute('data-subtitles')
  ) {
    if (this.el_.hasAttribute('data-captions')) {
      showCaptions = true;
    }
    if (this.el_.hasAttribute('data-subtitles')) {
      showSubtitles = true;
    }
    // fallback for unset data-attributes
  } else {
    const tracks = player.textTracks();

    for (var tt = 0; tt < tracks.length; tt++) {
      var track = tracks[tt];
      if (track.kind === 'subtitles') {
        if (track.label === 'English') {
          showCaptions = true;
        } else {
          showSubtitles = true;
        }
      }
    }
  }

  if (showSubtitles) {
    applyMenu(buildMenus(subtitlesLabel));
  }

  if (showCaptions) {
    applyMenu(buildMenus(captionsLabel, true));
  }

  const desktopShowMenu = player.el_.querySelectorAll(
    '.vjs-subs-caps-button--desktop-show',
  );

  if (showSubtitles && showCaptions) {
    player.el_.querySelector(
      '.vjs-subs-caps-button--mobile-show .vjs-menu-item.vjs-texttrack-settings .vjs-menu-item-text',
    ).innerText = captionSubtitlesSettingsLabel;
    player.el_.querySelector(
      '.vjs-subs-caps-button--mobile-show .vjs-menu-item:not(.vjs-texttrack-settings):not(vjs-subtitles-menu-item) .vjs-menu-item-text',
    ).innerText = captionSubtitlesOffLabel;
  }

  for (const menu in desktopShowMenu) {
    if (Object.hasOwnProperty.call(desktopShowMenu, menu)) {
      const menuElement = desktopShowMenu[menu];
      if (menuElement.classList.contains('vjs-subs-caps-button--captions')) {
        removeListItems(menuElement, true);
      } else {
        removeListItems(menuElement);
      }
    }
  }
});
