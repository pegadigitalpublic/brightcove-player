videojs.registerPlugin('emailSocialShare', function (show) {
  if (show) {
    var player = this;
    var image =
      '<svg width="16px" height="16px" viewBox="0 0 16 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="position:relative; top:1px; margin:-2px; z-index: -100">' +
      '<g id="Font-Exercise" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">' +
      '<g id="Email" fill="#000000">' +
      '<path d="M14.2702703,2 C15.2216216,2 16,2.7902439 16,3.75609756 L16,12.2439024 C16,13.2097561 15.2216216,14 14.2702703,14 L1.72972973,14 C0.778378378,14 0,13.2097561 0,12.2439024 L0,3.75609756 C0,2.7902439 0.778378378,2 1.72972973,2 L14.2702703,2 Z M13.6948468,11.8847805 C13.8891532,11.6820976 13.8848288,11.3578049 13.6851892,11.1603902 L10.6384144,8.14985366 L13.5427748,5.25229268 C13.7415495,5.054 13.7442883,4.72970732 13.548973,4.52804878 C13.3536577,4.32639024 13.0342342,4.32360976 12.8356036,4.52160976 L8,9.34590244 L3.1643964,4.52160976 C2.96562162,4.32346341 2.64634234,4.3262439 2.45088288,4.52804878 C2.25571171,4.72970732 2.25845045,5.054 2.45722523,5.25229268 L5.36144144,8.14985366 L2.31466667,11.1603902 C2.11517117,11.3578049 2.1107027,11.6820976 2.30500901,11.8847805 C2.40403604,11.9879512 2.53520721,12.0397561 2.66666667,12.0397561 C2.79351351,12.0397561 2.92036036,11.9914634 3.01852252,11.8945854 L6.0812973,8.86809756 L7.64641441,10.4294146 C7.74443243,10.5273171 7.87214414,10.5763415 8,10.5763415 C8.12771171,10.5763415 8.25542342,10.5273171 8.35358559,10.4294146 L9.91855856,8.86809756 L12.9813333,11.8945854 C13.0794955,11.9914634 13.2063423,12.0397561 13.3331892,12.0397561 C13.4646486,12.0397561 13.595964,11.9879512 13.6948468,11.8847805 Z" id="Fill-1"></path>' +
      '</g>' +
      '</g>' +
      '</svg>';

    var createMailtoLink = function createMailtoLink(attributes) {
      var videoUrl = attributes['data-email-videourl']
        ? attributes['data-email-videourl'].value
        : window.location.href;
      var body =
        (attributes['data-email-body']
          ? attributes['data-email-body'].value + '\n'
          : '') + videoUrl;
      var subject = attributes['data-email-subject']
        ? attributes['data-email-subject'].value
        : '';
      return (
        'mailto:' +
        '?subject=' +
        encodeURIComponent(subject) +
        '&body=' +
        encodeURIComponent(body)
      );
    };

    var shouldAddIcon = function shouldAddIcon(player, socialLinks) {
      var emailSocialLink =
        socialLinks.getElementsByClassName('email-share-link')[0];
      var videoUrl =
        player.el_.attributes['data-email-videourl'] &&
        player.el_.attributes['data-email-videourl'].value;
      if (!videoUrl) {
        videoUrl = window.location.href;
      }
      return !emailSocialLink && videoUrl;
    };

    var createEmailIcon = function createEmailIcon(attributes, player) {
      var link = createMailtoLink(attributes);
      var videoUrl = attributes['data-email-videourl']
        ? attributes['data-email-videourl'].value
        : window.location.href;
      return (
        '<a href="' +
        link +
        '" data-email-videoid="' +
        player.mediainfo.id +
        '" data-email-videotitle="' +
        player.mediainfo.name +
        '" data-email-videourl="' +
        videoUrl +
        '" class="vjs-social-share-link share-icon-email" style="background-color:#FFFFFF; position:relative; z-index: 100" aria-label="Share by email" target="_blank" title="Email">' +
        image +
        '<span style="position:absolute; width:100%; height:100%; z-index: 0" >' +
        '<span class="vjs-control-text">email</span>' +
        '</a>'
      );
    };

    var sendEmail = function sendEmail(e) {
      e.preventDefault();
      e.stopPropagation();
      var player = this;
      var href = player.el_.getElementsByClassName('share-icon-email')[0].href;
      window.open(href, '_blank');
      var videoEmailShareEvent = document.createEvent('Event');
      videoEmailShareEvent.initEvent('videoEmailShareEvent', true, true);
      videoEmailShareEvent.href =
        player.el_.getElementsByClassName('share-icon-email')[0].href;
      videoEmailShareEvent.url =
        player.el_.getElementsByClassName(
          'share-icon-email'
        )[0].dataset.emailVideourl;
      videoEmailShareEvent.title =
        player.el_.getElementsByClassName(
          'share-icon-email'
        )[0].dataset.emailVideotitle;
      videoEmailShareEvent.id =
        player.el_.getElementsByClassName(
          'share-icon-email'
        )[0].dataset.emailVideoid;
      document.dispatchEvent(videoEmailShareEvent);
    };

    var setEmailShareButton = function setEmailShareButton(player) {
      var socialLinks = player.el_.getElementsByClassName(
        'vjs-social-share-links'
      )[0];
      if (shouldAddIcon(player, socialLinks)) {
        socialLinks.setAttribute('style', 'overflow-y: hidden');
        socialLinks.innerHTML += createEmailIcon(player.el_.attributes, player);
        // Workaround not to trigger other plugin events.
        player.el_
          .getElementsByClassName('share-icon-email')[0]
          .addEventListener('click', sendEmail.bind(player));
      }
    };

    var addShareButtonListener = function addShareButtonListener(player) {
      var socialOverlayButton =
        player.el_.getElementsByClassName('vjs-share-control')[0];
      socialOverlayButton.addEventListener('click', function () {
        setEmailShareButton(player);
      });
      player.on('ended', function setEmailHook() {
        setEmailShareButton(player);
      });
      socialOverlayButton.addEventListener('touchend', function setEmailHook() {
        setEmailShareButton(player);
      });
    };

    if (!player.activePlugins_.social) {
      console.error(
        'WARNING - social plugin missing - emailSocialShare plugin will not take effect.'
      );
      return;
    }

    //pluginsetup
    setTimeout(function addShareButtonListenerCallback() {
      addShareButtonListener(player);
    }, 100);
  }
});
