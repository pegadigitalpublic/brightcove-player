videojs.registerPlugin('timeStampQuery', function (params) {
  const videoid = params.videoid;
  const timestamp = params.timestamp;

  const targetVideo = document.querySelector(
    'video[data-video-id="' + videoid + '"], video[data-delayed-video-id="' + videoid + '"]'
  );

  if (targetVideo) {
    // set timestamp no matter what
    targetVideo.currentTime = timestamp;

    // if the browser allows autoplay, play the video. don't allow on
    // muted when browser allows muted or disallow autoplay all together.
    if (navigator.getAutoplayPolicy(targetVideo) === 'allowed') {
      // if there is a thumbnail, we want to make the video container visible and remove the button.
      // ref: pega_bolt_theme/bolt/packages/components/bolt-video-thumbnail/src/video-thumbnail.js
      const targetThumbnail = targetVideo.closest('.c-bolt-video-thumbnail');
      if (targetThumbnail) {
        const targetThumbnailContainer = targetThumbnail.querySelector(
          '.js-bolt-video-thumbnail-content-video'
        );
        const targetThumbnailButton = targetThumbnail.querySelector(
          '.js-bolt-video-thumbnail-inline-button'
        );

        if (targetThumbnailContainer) {
          targetThumbnailContainer.style.display = 'block';
        }

        if (targetThumbnailButton) {
          targetThumbnailButton.remove();
        }
      }

      targetVideo.play();
      targetVideo.focus();
    }
  }
});
