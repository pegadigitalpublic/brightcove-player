videojs.registerPlugin('ga', function (options) {
  var adStateRegex,
    currentVideo,
    dataSetupOptions,
    defaultLabel,
    defaultsEventsToTrack,
    end,
    endTracked,
    eventCategory,
    eventLabel,
    eventNames,
    eventsToTrack,
    fullscreen,
    getEventName,
    getFormattedError,
    isInAdState,
    loaded,
    parsedOptions,
    pause,
    percentsAlreadyTracked,
    percentsPlayedInterval,
    play,
    player,
    playerEl,
    playerMode,
    isBackgroundVideo,
    formatEventName,
    referrer,
    resize,
    seekEnd,
    seekStart,
    seeking,
    sendbeacon,
    sendbeaconOverride,
    start,
    startTracked,
    timeupdate,
    tracker,
    trackerName,
    volumeChange,
    getEventLabel,
    shareButtonClick,
    shareLinkClick,
    videoQualityChange,
    closedCaptioningClick,
    ctaClick,
    replay,
    paused,
    errorEvent,
    _this = this;
  var __indexOf =
    [].indexOf ||
    function (item) {
      for (var i = 0, l = this.length; i < l; i++) {
        if (i in this && this[i] === item) return i;
      }
      return -1;
    };

  if (options == null) {
    options = {};
  }
  referrer = document.createElement('a');
  referrer.href = document.referrer;
  if (
    self !== top &&
    window.location.host === 'preview-players.brightcove.net' &&
    referrer.hostname === 'studio.brightcove.com'
  ) {
    videojs.log(
      'Google analytics plugin will not track events in Video Cloud Studio'
    );
    return;
  }
  player = this;
  // Get player's HTML node.
  playerEl = player.el_;
  playerMode = player.el_.classList.contains('c-base-video--audio-only') ? 'audio' : 'video';
  // Check if video is added as background video.
  isBackgroundVideo = (playerEl.closest('bolt-background') || playerEl.closest('.c-bolt-background')) !== null;
  formatEventName = function(eventName) {
    var output = '';
    switch (playerMode) {
      case 'audio':
        output = 'AUDIO - ' + eventName;
        break;
      default:
        output = 'VID - ' + eventName;
    }
    return output;
  }

  // Fallback to run code was previously included within current plugin and now
  // is separate 'lmms' plugin. Initialize 'lmms' plugin in case if Drupal
  // module is not up to date, and in result did not enable LMMS tracking.
  // Todo: Remove the code once all Drupal based projects update pega_bolt module.
  if (typeof this.activePlugins_ !== 'undefined' &&
    typeof this.activePlugins_['lmms'] === 'undefined' &&
    typeof this.lmms === 'function'
  ) {
    this.lmms();
  }

  dataSetupOptions = {};
  if (this.options()['data-setup']) {
    parsedOptions = JSON.parse(this.options()['data-setup']);
    if (parsedOptions.ga) {
      dataSetupOptions = parsedOptions.ga;
    }
  }
  defaultsEventsToTrack = [
    'player_load',
    'video_load',
    'percent_played',
    'start',
    'end',
    'seek',
    'play',
    'pause',
    'resize',
    'volume_change',
    'fullscreen',
    'error',
  ];
  eventsToTrack =
    options.eventsToTrack ||
    dataSetupOptions.eventsToTrack ||
    defaultsEventsToTrack;
  percentsPlayedInterval =
    options.percentsPlayedInterval ||
    dataSetupOptions.percentsPlayedInterval ||
    25;
  eventCategory =
    options.eventCategory ||
    dataSetupOptions.eventCategory ||
    'Brightcove Player';
  defaultLabel = options.eventLabel || dataSetupOptions.eventLabel;
  sendbeaconOverride = options.sendbeaconOverride || false;
  options.debug = options.debug || false;
  options.trackerName = options.trackerName || null;
  trackerName = '';
  if (typeof options.trackerName === 'string') {
    trackerName = options.trackerName + '.';
  }
  percentsAlreadyTracked = [];
  startTracked = false;
  endTracked = false;
  seekStart = seekEnd = 0;
  seeking = false;
  paused = false;
  eventLabel = '';
  currentVideo = '';
  eventNames = {
    video_load: 'Video Load',
    percent_played: 'Percent played',
    start: 'Media Begin',
    seek_start: 'Seek start',
    seek_end: 'Seek end',
    play: 'Media Play',
    pause: 'Media Pause',
    fullscreen_exit: 'Fullscreen Exited',
    fullscreen_enter: 'Fullscreen Entered',
    resize: 'Resize',
    volume_change: 'Volume Change',
    player_load: 'Player Load',
    end: 'Media Complete',
  };
  getEventLabel = function (eventAction, href) {
    var evtLabel;
    switch (eventAction) {
      case 'VID - Social click':
      case 'VID - CTA click':
        evtLabel =
          player.mediainfo.name + ' | ' + player.mediainfo.id + ' | ' + href;
        break;
      default:
        evtLabel = player.mediainfo.name + ' | ' + player.mediainfo.id;
        break;
    }
    return evtLabel;
  };
  getEventName = function (name) {
    if (options.eventNames && options.eventNames[name]) {
      return options.eventNames[name];
    }
    if (dataSetupOptions.eventNames && dataSetupOptions.eventNames[name]) {
      return dataSetupOptions.eventNames[name];
    }
    if (eventNames[name]) {
      return eventNames[name];
    }
    return name;
  };
  if (
    window.location.host === 'players.brightcove.net' ||
    window.location.host === 'preview-players.brightcove.net' ||
    trackerName !== ''
  ) {
    tracker = options.tracker || dataSetupOptions.tracker;
    if (tracker) {
      (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] =
          i[r] ||
          function () {
            return (i[r].q = i[r].q || []).push(arguments);
          };
        i[r].l = 1 * new Date();
        a = s.createElement(o);
        m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        return m.parentNode.insertBefore(a, m);
      })(
        window,
        document,
        'script',
        '//www.google-analytics.com/analytics.js',
        'ga'
      );
      ga('create', tracker, 'auto', options.trackerName);
      ga(trackerName + 'require', 'displayfeatures');
    }
  }
  adStateRegex = /(\s|^)vjs-ad-(playing|loading)(\s|$)/;
  isInAdState = function (player) {
    return adStateRegex.test(player.el().className);
  };
  loaded = function () {
    if (!isInAdState(player)) {
      if (defaultLabel) {
        eventLabel = defaultLabel;
      } else {
        if (player.mediainfo && player.mediainfo.id) {
          eventLabel = player.mediainfo.id + ' | ' + player.mediainfo.name;
        } else {
          eventLabel = this.currentSrc()
            .split('/')
            .slice(-1)[0]
            .replace(/\.(\w{3,4})(\?.*)?$/i, '');
        }
      }
      if (
        player.mediainfo &&
        player.mediainfo.id &&
        player.mediainfo.id !== currentVideo
      ) {
        currentVideo = player.mediainfo.id;
        percentsAlreadyTracked = [];
        startTracked = false;
        endTracked = false;
        seekStart = seekEnd = 0;
        seeking = false;
        if (__indexOf.call(eventsToTrack, 'video_load') >= 0) {
          sendbeacon(
            'asset engagement',
            formatEventName('Player Load'),
            getEventLabel(formatEventName('Player Load')),
            true,
            2
          );
        }
      }
    }
  };
  timeupdate = function () {
    var currentTime, duration, percent, percentPlayed, _i;
    if (!isInAdState(player)) {
      currentTime = Math.round(this.currentTime());
      duration = Math.round(this.duration());
      percentPlayed = Math.round((currentTime / duration) * 100);
      for (
        percent = _i = 0;
        _i <= 100;
        percent = _i += percentsPlayedInterval
      ) {
        if (
          percentPlayed >= percent &&
          __indexOf.call(percentsAlreadyTracked, percent) < 0
        ) {
          if (
            __indexOf.call(eventsToTrack, 'percent_played') >= 0 &&
            percentPlayed !== 0 &&
            percent !== 0
          ) {
            sendbeacon(
              'asset engagement',
              formatEventName('Milestone'),
              getEventLabel(formatEventName('Milestone')),
              true,
              percent
            );
          }
          if (percentPlayed > 0) {
            percentsAlreadyTracked.push(percent);
          }
        }
      }
      if (__indexOf.call(eventsToTrack, 'seek') >= 0) {
        seekStart = seekEnd;
        seekEnd = currentTime;
        if (seekStart - seekEnd < -1) {
          sendbeacon(
            'asset engagement',
            formatEventName('Scrubber Forward'),
            getEventLabel(formatEventName('Scrubber Forward')),
            false,
            currentTime
          );
        } else if (seekStart - seekEnd > 1) {
          sendbeacon(
            'asset engagement',
            formatEventName('Scrubber Back'),
            getEventLabel(formatEventName('Scrubber Back')),
            false,
            currentTime
          );
        }
        if (Math.abs(seekStart - seekEnd) > 1) {
          seeking = true;
          //sendbeacon(getEventName('seek_start'), false, seekStart);
          //sendbeacon(getEventName('seek_end'), false, seekEnd);
        }
      }
    }
  };
  rateChange = function () {
    sendbeacon(
      'asset engagement',
      formatEventName('Speed Change'),
      this.mediainfo.name + ' | ' + this.mediainfo.id,
      false,
      this.playbackRate() * 10
    );
  };
  end = function () {
    if (!isInAdState(player)) {
      sendbeacon(
        'interact',
        formatEventName('Complete'),
        getEventLabel(formatEventName('Complete')),
        true,
        2
      );
      seekStart = seekEnd = 0;
      paused = false;
      //endTracked = true;
    }
  };
  play = function () {
    var currentTime;
    if (!isInAdState(player)) {
      currentTime = Math.round(this.currentTime());
      if (paused) {
        sendbeacon(
          'asset engagement',
          formatEventName('Resume'),
          getEventLabel(formatEventName('Resume')),
          false,
          Math.round((player.currentTime() / player.duration()) * 100)
        );
      } else {
        sendbeacon(
          'interact',
          formatEventName('Play'),
          getEventLabel(formatEventName('Play')),
          false,
          2
        );
      }
      paused = false;
      seeking = false;
    }
  };
  start = function () {
    if (!isInAdState(player)) {
      if (__indexOf.call(eventsToTrack, 'start') >= 0 && !startTracked) {
        //sendbeacon(getEventName('start'), true);
        return (startTracked = true);
      }
    }
  };
  pause = function () {
    var currentTime, duration;
    paused = true;
    if (!isInAdState(player)) {
      currentTime = Math.round(this.currentTime());
      duration = Math.round(this.duration());
      if (currentTime !== duration && !seeking) {
        sendbeacon(
          'asset engagement',
          formatEventName('Pause'),
          getEventLabel(formatEventName('Pause')),
          false,
          Math.round((player.currentTime() / player.duration()) * 100)
        );
      }
    }
  };
  volumeChange = function () {
    var volume;
    volume = this.muted() === true ? 0 : this.volume();
    sendbeacon(
      'asset engagement',
      formatEventName('Volume Change'),
      getEventLabel(formatEventName('Volume Change')),
      false,
      Math.floor(volume * 100)
    );
  };
  resize = function () {
    //sendbeacon(getEventName('resize') + ' - ' + this.width() + "*" + this.height(), true);
  };
  fullscreen = function () {
    var currentTime;
    currentTime = Math.round(this.currentTime());
    if (this.isFullscreen()) {
      sendbeacon(
        'asset engagement',
        formatEventName('Fullscreen click'),
        getEventLabel(formatEventName('Fullscreen click')),
        false,
        2
      );
    } else {
      //sendbeacon(getEventName('fullscreen_exit'), false, currentTime);
    }
  };
  errorEvent = function () {
    sendbeacon(
      'asset engagement',
      formatEventName('Error'),
      getEventLabel(formatEventName('Error')),
      true,
      Math.round((player.currentTime() / player.duration()) * 100)
    );
  };
  shareButtonClick = function () {
    sendbeacon(
      'asset engagement',
      formatEventName('Share click'),
      getEventLabel(formatEventName('Share click')),
      false,
      2
    );
  };
  shareLinkClick = function (evt) {
    if (
      ['Facebook', 'Google+', 'LinkedIn', 'Twitter'].indexOf(evt.target.title) >
      -1
    ) {
      sendbeacon(
        'asset engagement',
        formatEventName('Social click'),
        getEventLabel(formatEventName('Social click'), evt.target.href),
        false,
        2
      );
    }
    if (evt.target.textContent === 'Restart') replay();
  };
  videoQualityChange = function (player, resolutionChange) {
    sendbeacon(
      'asset engagement',
      formatEventName('Quality Change'),
      player.mediainfo.name + ' | ' + player.mediainfo.id,
      false,
      resolutionChange
    );
  };
  closedCaptioningClick = function (player, captionOption) {
    sendbeacon(
      'asset engagement',
      formatEventName('Closed Caption Select'),
      player.mediainfo.name +
        ' | ' +
        player.mediainfo.id +
        ' | ' +
        captionOption,
      false,
      2
    );
  };
  ctaClick = function (evt) {
    var aTag = evt.currentTarget.getElementsByTagName('a')[0],
      link = aTag.getAttribute('href');
    sendbeacon(
      'interact',
      formatEventName('CTA click'),
      getEventLabel(formatEventName('CTA click'), link),
      false,
      2
    );
  };
  replay = function () {
    sendbeacon(
      'asset engagement',
      formatEventName('Replay'),
      getEventLabel(formatEventName('Replay')),
      false,
      2
    );
  };
  sendbeacon = function (
    eventCategory,
    action,
    eventLabel,
    nonInteraction,
    value
  ) {
    // Don't track video background.
    if (isBackgroundVideo) {
      return;
    }

    if (typeof Drupal !== 'undefined') {
      if (Drupal && Drupal.pegaAnalytics && Drupal.pegaAnalytics.gaSendEvent) {
        Drupal.pegaAnalytics.gaSendEvent(
          eventCategory,
          action,
          eventLabel,
          value,
          nonInteraction
        );
      }
    }
    if (sendbeaconOverride) {
      sendbeaconOverride(
        eventCategory,
        action,
        eventLabel,
        value,
        nonInteraction
      );
    } else if (window.ga) {
      ga(trackerName + 'send', 'event', {
        eventCategory: eventCategory,
        eventAction: action,
        eventLabel: eventLabel,
        eventValue: value,
        nonInteraction: nonInteraction,
      });
    } else if (window._gaq) {
      _gaq.push([
        '_trackEvent',
        eventCategory,
        action,
        eventLabel,
        value,
        nonInteraction,
      ]);
    } else if (options.debug) {
      videojs.log('Google Analytics not detected');
    }
  };
  /**
   * Get formatted error for sendbeacon format.
   *
   * @param playbackResponse
   *   Brightcove playback response.
   *
   * @returns {[string,string,string,boolean,string]}
   */
  getFormattedError = function(playbackResponse) {
    return [
      'asset engagement',
      'VID - Error',
      player.mediainfo.name + ' | ' + player.mediainfo.id,
      true,
      new Date() +
      ' | ' +
      player.error().code +
      ' | ' +
      player.error().message +
      ' | ' +
      player.mediainfo.accountId +
      ' | ' +
      player.mediainfo.id +
      ' | ' +
      player.mediainfo.playerId +
      ' | ' +
      playbackResponse.responseURL +
      ', ' +
      playbackResponse.responseType +
      ', ' +
      playbackResponse.response
    ];
  }
  this.ready(function () {
    var href, iframe;
    this.on('loadedmetadata', loaded);
    this.on('timeupdate', timeupdate);
    if (__indexOf.call(eventsToTrack, 'end') >= 0) {
      this.on('ended', end);
    }
    if (__indexOf.call(eventsToTrack, 'play') >= 0) {
      this.on('play', play);
    }
    if (__indexOf.call(eventsToTrack, 'start') >= 0) {
      this.on('playing', start);
    }
    if (__indexOf.call(eventsToTrack, 'pause') >= 0) {
      this.on('pause', pause);
    }
    if (__indexOf.call(eventsToTrack, 'volume_change') >= 0) {
      this.on('volumechange', volumeChange);
    }
    if (__indexOf.call(eventsToTrack, 'resize') >= 0) {
      this.on('resize', resize);
    }
    if (__indexOf.call(eventsToTrack, 'fullscreen') >= 0) {
      this.on('fullscreenchange', fullscreen);
    }
    if (__indexOf.call(eventsToTrack, 'error') >= 0) {
      this.on('error', errorEvent);
    }
    this.on('ratechange', rateChange.bind(this));

    if (this.socialButton) {
      this.socialButton.on('click', shareButtonClick);
    }

    if (this.socialOverlay) {
      this.socialOverlay.on('click', shareLinkClick);
    }
    if (player.overlays_ && player.overlays_.length > 0) {
      for (var i = 0; i < player.overlays_.length; i++) {
        player.overlays_[i].on('click', ctaClick);
      }
    }
  });
  this.on('loadedmetadata', function () {
    const languages = this.el_.querySelectorAll(
      '.vjs-subs-caps-button .vjs-menu-item'
    );

    for (let l = 0; l < languages.length; l++) {
      let language = languages[l];
      language.addEventListener(
        'click',
        function (e) {
          const language = e.currentTarget.querySelector('.vjs-menu-item-text');
          const stringClean = /\n\s+/;
          let currentLangCode = this.language_;
          if (typeof this.languages_[this.language_] === 'undefined') {
            let currentLangCodeParsed = currentLangCode.match(/^(\w+)-/);
            if (currentLangCodeParsed) {
              currentLangCode = currentLangCodeParsed[1]
            }
          }
          let languageText = language.innerText
            .replace(this.languages_[currentLangCode].Captions, '')
            .replace(stringClean, '');
          if (
            languageText === this.languages_[currentLangCode]['captions off']
          ) {
            languageText = 'Off';
          }
          if (
            languageText !==
            this.languages_[currentLangCode]['captions settings']
          ) {
            closedCaptioningClick(this, languageText);
          }
        }.bind(this)
      );
    }

    const qualityOptions = this.el_.querySelectorAll(
      '.vjs-quality-menu-wrapper .vjs-menu .vjs-menu-item'
    );

    for (let l = 0; l < qualityOptions.length; l++) {
      const qualityOption = qualityOptions[l];
      qualityOption.addEventListener(
        'click',
        function (e) {
          const quality = e.currentTarget.querySelector('.vjs-menu-item-text');
          const qualityText = quality.innerText;
          videoQualityChange(this, qualityText);
        }.bind(this)
      );
    }
  });

  return {
    sendbeacon: sendbeacon,
    getFormattedError: getFormattedError
  };
});
